import math
def lagr(n):
	"""
	compute the largest prime factor of n
	"""
	if n %2 == 0:
		n=n/2
		lastfactor = 2
		while n %2 == 0:
			n=n/2
	else:
		lastfactor = 1
	
	factor = 3
	maxfactor = math.sqrt(n)
	while n>1 and factor <= maxfactor:
		if n%factor == 0:
			n/=factor
			lastfactor = factor
			while n%factor ==0:
				n/=factor
			maxfactor=math.sqrt(n)

		factor+=2

	if n==1:
		return lastfactor
	else:
		return n
print lagr(100)
print lagr(42376523)
print lagr(600851475143)
